export async function signInUser (body) {
  const response = await window.fetch(`${process.env.REACT_APP_URL}/auth/login`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': `${process.env.FRONTEND_URL}`
    },
    credentials: 'include',
    body: JSON.stringify(body)
  })
  const data = await response.json()
  return { status: response.status, data: data }
}

export async function registerUser (body) {
  const response = await window.fetch(`${process.env.REACT_APP_URL}/auth/register`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': `${process.env.FRONTEND_URL}`
    },
    credentials: 'include',
    body: JSON.stringify(body)
  })
  const data = await response.json()
  return { status: response.status, data: data }
}

export async function bookClassRequest (body) {
  const response = await window.fetch(`${process.env.REACT_APP_URL}/classes/${body.classID}`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': `${process.env.FRONTEND_URL}`
    },
    body: JSON.stringify(body)
  })
  const data = await response.json()
  return { status: response.status, data: data }
}

export async function cancelClassRequest (body) {
  const response = await window.fetch(`${process.env.REACT_APP_URL}/classes/${body.classID}/cancel`, {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': `${process.env.FRONTEND_URL}`
    },
    body: JSON.stringify(body)
  })
  const data = await response.json()
  return { status: response.status, data: data }
}

export async function changeClassStatus (body) {
  const response = await window.fetch(`${process.env.REACT_APP_URL}/classes/${body.classID}`, {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': `${process.env.FRONTEND_URL}`
    }
  })
  const data = await response.json()
  return { status: response.status, data: data }
}

export async function addClassRequest (body) {
  const response = await window.fetch(`${process.env.REACT_APP_URL}/classes/`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': `${process.env.FRONTEND_URL}`
    },
    body: JSON.stringify(body)
  })
  const data = await response.json()
  return { status: response.status, data: data }
}

export async function deleteClassRequest (body) {
  const response = await window.fetch(`${process.env.REACT_APP_URL}/classes/${body.classID}`, {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': `${process.env.FRONTEND_URL}`
    },
    body: JSON.stringify(body)
  })
  const data = await response.json()
  return { status: response.status, data: data }
}

export async function addTemplateClassRequest (body) {
  const response = await window.fetch(`${process.env.REACT_APP_URL}/template-class/`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': `${process.env.FRONTEND_URL}`
    },
    body: JSON.stringify(body)
  })
  const data = await response.json()
  return { status: response.status, data: data }
}

export async function deleteTemplateClassRequest (ClassID) {
  const response = await window.fetch(`${process.env.REACT_APP_URL}/template-class/${ClassID}`, {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': `${process.env.FRONTEND_URL}`
    }
  })
  const data = await response.json()
  return { status: response.status, data: data }
}
