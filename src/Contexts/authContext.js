import { createContext, useState, React } from 'react'

export const AuthContext = createContext()

export const AuthProvider = (props) => {
  const user = JSON.parse(window.localStorage.getItem('user'))
  const isAdminInLocalStorage = user ? user.isAdmin : false
  const [isAdmin, setIsAdmin] = useState(isAdminInLocalStorage)
  return (
    <AuthContext.Provider value={[isAdmin, setIsAdmin]}>
      {props.children}
    </AuthContext.Provider>
  )
}
