import React, { createContext, useState } from 'react'

export const BookClassContext = createContext()

export const BookClassProvider = (props) => {
  const [days, setDays] = useState([])
  return (
    <BookClassContext.Provider value={[days, setDays]}>
      {props.children}
    </BookClassContext.Provider>
  )
}
