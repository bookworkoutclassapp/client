/* import React, { createContext, useState, useMemo } from 'react'

export const TemplateClassesContext = createContext()

export const TemplateClassesProvider = (props) => {
  const [templateClasses, setTemplateClasses] = useState([])

  return (
    <TemplateClassesContext.Provider value={[templateClasses, setTemplateClasses]}>
      {props.children}
    </TemplateClassesContext.Provider>
  )
} */

import React, { createContext, useState, useMemo } from 'react'

export const TemplateClassesContext = createContext()

export const TemplateClassesProvider = (props) => {
  const [templateClasses, setTemplateClasses] = useState([])

  const sorted = useMemo(() => templateClasses.sort((a, b) => {
    return new Date(`1970-01-01T${a.startTime}`) - new Date(`1970-01-01T${b.startTime}`)
  }), [templateClasses])

  return (
    <TemplateClassesContext.Provider value={[sorted, setTemplateClasses]}>
      {props.children}
    </TemplateClassesContext.Provider>
  )
}
