import React, { useState } from 'react'
import style from './SignIn.module.css'
import { signInUser } from '../../apiCalls'
import Form from 'react-bootstrap/Form'
import Logo from '../../Components/Logo/Logo'
import NotAMember from '../../Components/NotAMember/NotAMember'
import { FaLine } from 'react-icons/fa'
import { Link } from 'react-router-dom'
import { useHistory } from 'react-router'
import { toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import { CircularProgress } from '@material-ui/core'
import Button from '../../Components/Button/Button'
import LockOpenIcon from '@material-ui/icons/LockOpen'
import TextField from '@material-ui/core/TextField'

toast.configure()
function SignIn () {
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [isLoading, setIsLoading] = useState(false)
  const history = useHistory()

  const notifyError = (message) => {
    toast.error(message, {
      autoClose: 2000
    })
  }

  const handleSubmit = async (event) => {
    setIsLoading(true)
    const body = {
      email: email,
      password: password
    }

    event.preventDefault()
    const response = await signInUser(body)

    if (response.status === 200) {
      const storage = {
        isLoggedIn: true,
        userID: response.data.user.userID,
        displayName: response.data.user.displayName,
        isAdmin: response.data.user.isAdmin
      }
      window.localStorage.setItem('user', JSON.stringify(storage))
      response.data.user.isAdmin ? history.push('adminDashboard/schedule') : history.push('userDashboard/book-class')
    }
    if (response.status === 401) {
      notifyError('No matching user!')
    }
    setIsLoading(false)
  }
  return (
    <header className='App-header'>
      <Logo />
      <NotAMember className='notAMemberStyle' />
      <h3 className={style.signInh3}>Sign in with</h3>
      <a href={`${process.env.REACT_APP_URL}/auth/line`}>
        <div>
          <FaLine className={style.icon} />
        </div>
      </a>
      <h3 className={style.or}>or</h3>
      <Form onSubmit={handleSubmit}>

        <TextField
          type='email'
          required
          value={email}
          style={{
            marginBottom: '10px'
          }}
          fullWidth
          label='Email'
          onChange={(e) => setEmail(e.target.value)}
        />

        <TextField
          type='password'
          required
          value={password}
          label='Password'
          onChange={(e) => setPassword(e.target.value)}
          fullWidth
          style={{
            marginBottom: '10px'
          }}
        />

        <Link className={style.forgot} to='./forgot-password'>Forgot Password?</Link>
        <Button
          disabled={!email || !password || isLoading}
          startIcon={isLoading ? null : <LockOpenIcon />}
          type='submit'
          text={isLoading
            ? <CircularProgress className='loader' color='secondary' />
            : 'Sign in'}
          width='300px'
        />
      </Form>
    </header>
  )
}

export default SignIn
