import React, { useState, useEffect, useContext } from 'react'
import Sidebar from '../../Components/Sidebar/Sidebar'
import Logout from '../../Components/Logout/Logout'
import Logo from '../../Components/Logo/Logo'
import DayTab from '../../Components/DayTab/DayTab'
import '../Schedule/Schedule.css'
import '../WeeklyTemplate/WeeklyTemplate.css'
import { CircularProgress } from '@material-ui/core'
import { TemplateClassesContext } from '../../Contexts/TemplateClassesContext'

function WeeklyTemplate () {
  const [isLoading, setIsLoading] = useState(true)
  const [templateClasses, setTemplateClasses] = useContext(TemplateClassesContext)

  const fetchClasses = async () => {
    await window.fetch(`${process.env.REACT_APP_URL}/template-class`)
      .then(res => res.json())
      .then(res => { setTemplateClasses(res.classes); setIsLoading(false) })
  }

  useEffect(() => {
    fetchClasses()
  }, [])

  return (
    <header className='App-header'>
      <Sidebar />
      <Logout />
      <Logo />
      {isLoading ? <CircularProgress className='loader' color='secondary' />
        : <>
          <DayTab day='Monday' />
          <DayTab day='Tuesday' />
          <DayTab day='Wednesday' />
          <DayTab day='Thursday' />
          <DayTab day='Friday' />
          <DayTab day='Saturday' />
          <DayTab day='Sunday' />
        </>}
    </header>
  )
}

export default WeeklyTemplate
