import React, { useState } from 'react'
import { registerUser } from '../../apiCalls'
import Form from 'react-bootstrap/Form'
import Button from '../../Components/Button/Button'
import Logo from '../../Components/Logo/Logo'
import AlreadyAMember from '../../Components/AlreadyAMember/AlreadyAMember'
import { useHistory } from 'react-router'
import { toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import { CircularProgress } from '@material-ui/core'
import TextField from '@material-ui/core/TextField'
import PersonAddIcon from '@material-ui/icons/PersonAdd'

toast.configure()
function Register () {
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [repeatPassword, setRepeatPassword] = useState('')
  const [nickname, setNickname] = useState('')
  const [instagram, setInstagram] = useState('')
  const history = useHistory()
  const [isLoading, setIsLoading] = useState(false)

  const notifySuccess = (message) => {
    toast.success(message, {
      autoClose: 2000
    })
  }
  const notifyError = (message) => {
    toast.error(message, {
      autoClose: 2000
    })
  }

  const handleSubmit = async (event) => {
    const body = {
      email: email,
      password: password,
      repeatPassword: repeatPassword,
      displayName: nickname,
      instagram: instagram
    }

    event.preventDefault()
    setIsLoading(true)
    const response = await registerUser(body)
    setIsLoading(false)
    if (response.status === 400) {
      notifyError('User already exists')
    }
    if (response.status === 201) {
      notifySuccess('Account created! You can now login')
      history.push('/sign-in')
    }
  }
  return (
    <header className='App-header'>
      <Logo />
      <AlreadyAMember />
      <Form onSubmit={handleSubmit}>

        <TextField
          type='email'
          required
          value={email}
          style={{
            marginTop: '25px',
            marginBottom: '20px'
          }}
          fullWidth
          label='Email'
          onChange={(e) => setEmail(e.target.value)}
        />

        <TextField
          type='password'
          required
          value={password}
          label='Password'
          onChange={(e) => setPassword(e.target.value)}
          fullWidth
          style={{
            marginBottom: '20px'
          }}
        />

        <TextField
          type='password'
          required
          value={repeatPassword}
          label='Repeat Password'
          onChange={(e) => setRepeatPassword(e.target.value)}
          fullWidth
          style={{
            marginBottom: '20px'
          }}
        />

        <TextField
          type='text'
          required
          value={nickname}
          label='Nickname'
          onChange={(e) => setNickname(e.target.value)}
          fullWidth
          style={{
            marginBottom: '20px'
          }}
        />

        <TextField
          type='text'
          value={instagram}
          label='Instagram (optional)'
          onChange={(e) => setInstagram(e.target.value)}
          fullWidth
          style={{
            marginBottom: '20px'
          }}
        />

        <Button
          disabled={!email || !password || !repeatPassword || !nickname || isLoading}
          className='registerButton'
          startIcon={isLoading ? null : <PersonAddIcon />}
          type='submit'
          text={isLoading
            ? <CircularProgress className='loader' color='secondary' />
            : 'Register'}
        />
      </Form>

    </header>
  )
}

export default Register
