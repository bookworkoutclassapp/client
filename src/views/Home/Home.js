import { React } from 'react'
import '../../App.css'
import { Link } from 'react-router-dom'
import Logo from '../../Components/Logo/Logo'
import MockClass from '../../Components/MockClass/MockClass'
import JoinTeam from '../../Components/JoinTeam/JoinTeam'
import HakimAvatar from '../../Components/HakimAvatar/HakimAvatar'
import Button from '../../Components/Button/Button'
import CalendarTodayIcon from '@material-ui/icons/CalendarToday'

function Home () {
  return (
    <header className='App-header'>
      <Logo />
      <div>
        <MockClass time='9:30' class='Calisthenics' />
        <MockClass time='10:45' class='Handstand' />
      </div>
      <HakimAvatar />
      <JoinTeam />
      <Link to='/sign-in'>
        <Button text='Book Class' startIcon={<CalendarTodayIcon width='307' />} />
      </Link>
    </header>
  )
}

export default Home
