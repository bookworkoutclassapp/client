import React, { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
import '../../App.css'
import './BookClass.css'
import moment from 'moment'
import Logo from '../../Components/Logo/Logo'
import Class from '../../Components/Class/Class'
import Logout from '../../Components/Logout/Logout'
import Sidebar from '../../Components/Sidebar/Sidebar'
import { toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import { CircularProgress } from '@material-ui/core'

toast.configure()
function BookClass () {
  const [days, setDays] = useState([])
  const [selectedDate, setSelectedDate] = useState(moment().format('YYYY-MM-DD'))
  const [isLoading, setIsLoading] = useState(true)
  const [isAdmin, setIsAdmin] = useState(false)

  const fetchClasses = () => {
    window.fetch(`${process.env.REACT_APP_URL}/classes`)
      .then(res => res.json())
      .then(res => { setDays(res); setIsLoading(false); setDefaultSelectedDate(res) })
  }

  const setAdmin = () => {
    const userVariable = JSON.parse(window.localStorage.getItem('user'))
    setIsAdmin(userVariable?.isAdmin)
  }

  const setDefaultSelectedDate = (arr) => {
      let result = (arr.filter(item => item.classes.length > 0))
      if(result.length !== 0) {
      setSelectedDate(moment(result[0].day).format('YYYY-MM-DD'))
      } 
  }

  useEffect(() => {
    fetchClasses()
    setAdmin()
  }, [])

  const classes = days.filter(day => {
    return moment(day.day).format('YYYY-MM-DD') === selectedDate
  })[0]

  const updateDaysAdd = (classID, startTime) => {
    const user = JSON.parse(window.localStorage.getItem('user'))
    const newParticipant = {
      userID: user.userID,
      displayName: user.displayName
    }
    const index = days.findIndex(function (item) {
      return moment(item.day).format('YYYY-MM-DD') === moment(startTime).format('YYYY-MM-DD')
    })
    const clone = days.slice()
    const classIndex = clone[index].classes.findIndex(function (item) {
      return item._id === classID
    })
    clone[index].classes[classIndex].participants.push(newParticipant)
    setDays(clone)
  }

  const updateDaysRemove = (classID, startTime) => { 
    const user = JSON.parse(window.localStorage.getItem('user'))
    const participantToRemove = {
      userID: user.userID,
      displayName: user.displayName
    }
    const index = days.findIndex(function (item) {
      return moment(item.day).format('YYYY-MM-DD') === moment(startTime).format('YYYY-MM-DD')
    })
    const clone = days.slice()
    const classIndex = clone[index].classes.findIndex(function (item) {
      return item._id === classID
    })
    const participants = clone[index].classes[classIndex].participants
    const participantsIndex = participants.findIndex(function (item) {
      return item.userID === participantToRemove.userID
    })
    clone[index].classes[classIndex].participants.splice(participantsIndex, 1)
    setDays(clone)
  }
  return (
    <header className='App-header'>
       {isAdmin ? <Sidebar /> : null}
        <Logout />
        <Logo />
      <ul className='BookClass--days'>
        {isLoading ? <CircularProgress className='loader' color='secondary'/>
          : days.map(day => (
            <li className='BookClass--day' key={day.day}>
              <p>{moment(day.day).format('dddd').substring(0, 3)}</p>
              <Link to={`/userDashboard/book-class/${moment(day.day).format('YYYY-MM-DD')}`}>
                <button disabled={day.classes.length === 0} className={`BookClass-date ${moment(day.day).format('YYYY-MM-DD') === selectedDate ? 'BookClass-activeDate' : ''}`} onClick={() => { setSelectedDate(moment(day.day).format('YYYY-MM-DD')) }}> {moment(day.day).format('D')}</button>
              </Link>
            </li>
          ))}
      </ul>
      <div>
        {classes && classes.classes.map(item => (
          <Class updateDaysAdd={updateDaysAdd} updateDaysRemove={updateDaysRemove} key={item._id} id={item._id} startTime={item.startTime} class={item.class} duration={item.duration} instructor={item.instructor} venue={item.venue} maxParticipants={item.maxParticipants} participants={item.participants} status={item.status} day={item.day} />
        ))}
      </div>

    </header>
  )
}

export default BookClass
