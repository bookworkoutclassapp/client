import React, { useState } from 'react'
import './ForgotPassword.css'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import Logo from '../../Components/Logo/Logo'
import NotAMember from '../../Components/NotAMember/NotAMember'

function SignIn () {
  const [email, setEmail] = useState('')

  function validateForm () {
    return email.length > 0
  }

  function handleSubmit (event) {
    event.preventDefault()
  }
  return (
    <header className='App-header'>
      <Logo />
      <NotAMember />
      <Form onSubmit={handleSubmit}>
        <Form.Group controlId='email' className='ForgotPassword__forgotPassDiv'>
          <Form.Control
            autoFocus
            type='email'
            placeholder='Email'
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
        </Form.Group>
        <Button type='submit' disabled={!validateForm()}>
          Reset password
        </Button>
      </Form>

    </header>
  )
}

export default SignIn
