import React, { useState } from 'react'
import Sidebar from '../../Components/Sidebar/Sidebar'
import Logout from '../../Components/Logout/Logout'
import Logo from '../../Components/Logo/Logo'
import AddClass from '../../Components/AddClass/AddClass'
import './Schedule.css'
import { IoMdArrowDropdown, IoMdArrowDropup } from 'react-icons/io'
import Classes from '../../Components/Classes/Classes'

function Schedule () {
  const [past, setPast] = useState(false)
  const [upcoming, setUpcoming] = useState(true)

  const showPast = () => {
    setPast(!past)
  }

  const showUpcoming = () => {
    setUpcoming(!upcoming)
  }

  const addNewClass = () => {
    setUpcoming('')
    setUpcoming(true)
  }

  return (
    <header className='App-header'>
      <Sidebar />
      <Logout />
      <Logo />
      <AddClass addNewClass={addNewClass} />
      <div className='Schedule--tab' onClick={showPast}>Past{past ? <IoMdArrowDropup color='#FFBA00' /> : <IoMdArrowDropdown color='#FFBA00' />}</div>
      {past
        ? <div><Classes url='past=true' /></div>
        : null}
      <div className='Schedule--tab ' onClick={showUpcoming}>Upcoming{upcoming ? <IoMdArrowDropup color='#FFBA00' /> : <IoMdArrowDropdown color='#FFBA00' />}</div>
      {upcoming
        ? <div><Classes url='upcoming=true' /></div>
        : null}

    </header>
  )
}

export default Schedule
