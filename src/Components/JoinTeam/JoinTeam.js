import React from 'react'
import style from './JoinTeam.module.css'

function JoinTeam (props) {
  return (
    <div>
      <h3>Join Team Hakim</h3>
      <p className={style.joinTeam}>I have been practicing and teaching handstand and calisthenics for more than 8 years. My group classes are for all levels. </p>
    </div>
  )
}

export default JoinTeam
