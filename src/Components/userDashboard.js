import React from 'react'
import { Redirect, Route, Switch } from 'react-router-dom'
import BookClass from '../views/BookClass/BookClass'

function UserDashboard () {
  return (
    <Switch>
      <Route path='/userDashboard/book-class' component={BookClass} />
      <Route path='/userDashboard/book-class/:date' component={BookClass} />
      <Redirect from='/userDashboard' to='/userDashboard/book-class' />
    </Switch>
  )
}

export default UserDashboard
