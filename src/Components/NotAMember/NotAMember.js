import React from 'react'
import { Link } from 'react-router-dom'
import style from './NotAMember.module.css'

function NotAMember () {
  return (
    <div>
      <p className={style.notAMember}>Not a member yet?
        <Link to='./register'>
          <span> Become one today</span>
        </Link>
      </p>
    </div>
  )
}

export default NotAMember
