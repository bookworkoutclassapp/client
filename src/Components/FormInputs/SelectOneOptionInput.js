
import React from 'react'
import {
  FormGroup,
  Input,
  Col
} from 'reactstrap'

function SelectOneOptionInput ({ props }) {
  const generateOptions = () => {
    return (props.options.map(option =>
      <option defaultChecked={props.defaultValue === option.id} key={option.id} value={option.value || option.id}>{option.name || option.company}</option>)
    )
  }

  const generateSelectNone = () => {
    return (
      <option key={props.selectNone} value=''>{props.selectNone}</option>
    )
  }

  return (
    <Col>
      <FormGroup>
        <label>{props.label}</label>
        <Input
          type='select'
          name={props.name}
          onChange={props.onChange}
          value={props.defaultValue ? props.defaultValue : ''}
        >
          <option key='default' value='' disabled defaultChecked={!props.defaultValue}>{props.selectLabel}</option>
          {props.selectNone ? generateSelectNone() : null}
          {generateOptions()}
        </Input>
      </FormGroup>
    </Col>
  )
}

export default SelectOneOptionInput
