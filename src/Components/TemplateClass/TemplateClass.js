import React, { useState, useContext, useEffect } from 'react'
import { TemplateClassesContext } from '../../Contexts/TemplateClassesContext'
import './TemplateClass.css'
import { deleteTemplateClassRequest } from '../../apiCalls'
import 'react-toastify/dist/ReactToastify.css'
import { toast } from 'react-toastify'

function TemplateClass (props) {
  const [templateClasses, setTemplateClasses] = useContext(TemplateClassesContext)
  const [classItem, setClassItem] = useState('')

  const fetchClassFromContext = () => {
    const classObj = templateClasses.filter(row => props.id === row._id)
    setClassItem(classObj)
  }

  useEffect(() => {
    fetchClassFromContext()
  }, [])

  const notifyError = (message) => {
    toast.error(message, {
      autoClose: 2000
    })
  }

  const removeClassFromContext = () => {
    return templateClasses.filter((item) => item._id !== props.id)
  }

  const deleteClass = async () => {
    const response = await deleteTemplateClassRequest(props.id)
    if (response.status === 200) {
      setTemplateClasses(removeClassFromContext())
    } else {
      notifyError('Something went wrong!')
    }
  }

  return (
    <div className='TemplateClass--ClassDiv'>
      <div className='TemplateClass--infoDiv'>
        <p><strong>{props.startTime} - {props.class}</strong></p>
        <p>{props.venue} - {props.duration} min</p>
        <p>{props.instructor}</p>
        <p>{props.maxParticipants} {props.maxParticipants === 1 ? ' student' : ' students'}</p>
      </div>
      <div className='AdminClass--actionDiv'>
        <div className='TemplateClass--actionBtn TemplateClass--cancel TemplateClass--cursor' onClick={deleteClass}>Delete</div>
      </div>
    </div>
  )
}

export default TemplateClass

/*

*/
