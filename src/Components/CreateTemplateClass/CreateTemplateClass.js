import React, { useState, useContext } from 'react'
import Form from 'react-bootstrap/Form'
import './CreateTemplateClass.css'
import { addTemplateClassRequest } from '../../apiCalls'
import { toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import { TemplateClassesContext } from '../../Contexts/TemplateClassesContext'
import { CircularProgress } from '@material-ui/core'
import TextField from '@material-ui/core/TextField'
import MenuItem from '@material-ui/core/MenuItem'
import Button from '../Button/Button'
import moment from 'moment'
import InputAdornment from '@material-ui/core/InputAdornment'

function CreateTemplateClass (props) {
  const [typeOfClass, setTypeOfClass] = useState('')
  const [day, setDay] = useState(props.day)
  const [startTime, setStartTime] = useState('')
  const [duration, setDuration] = useState(60)
  const [venue, setVenue] = useState('Siam Strength')
  const [instructor, setInstructor] = useState('Hakim Azahar')
  const [maxParticipants, setMaxParticipants] = useState(10)
  const [templateClasses, setTemplateClasses] = useContext(TemplateClassesContext)
  const [isLoading, setIsLoading] = useState(false)

  const notifyError = (message) => {
    toast.error(message, {
      autoClose: 2000
    })
  }

  const handleSubmit = async (event) => {
    setIsLoading(true)
    event.preventDefault()
    const body = {
      class: typeOfClass,
      day: day,
      startTime: startTime,
      duration: duration,
      venue: venue,
      instructor: instructor,
      maxParticipants: maxParticipants
    }

    const response = await addTemplateClassRequest(body)
    if (response.status === 201) {
      setTemplateClasses([...templateClasses, response.data.class])
      console.log(templateClasses)
      setIsLoading(false)
      props.closeDetailBar()
      setTypeOfClass('')
      setStartTime('')
    } else {
      notifyError('Something went wrong!')
    }
  }

  const typesOfClasses = [
    {
      value: 'Calisthenics',
      label: 'Calisthenics'
    },
    {
      value: 'Handstand',
      label: 'Handstand'
    },
    {
      value: 'Handstand Push Ups',
      label: 'Handstand Push Ups'
    },
    {
      value: 'Planche',
      label: 'Planche'
    }
  ]

  const weekdays = [
    {
      value: 'Monday',
      label: 'Monday'
    },
    {
      value: 'Tuesday',
      label: 'Tuesday'
    },
    {
      value: 'Wednesday',
      label: 'Wednesday'
    },
    {
      value: 'Thursday',
      label: 'Thursday'
    },
    {
      value: 'Friday',
      label: 'Friday'
    },
    {
      value: 'Saturday',
      label: 'Saturday'
    },
    {
      value: 'Sunday',
      label: 'Sunday'
    }
  ]

  const venues = [
    {
      value: 'Online',
      label: 'Online'
    },
    {
      value: 'Siam Strength',
      label: 'Siam Strength'
    }
  ]

  const instructors = [
    {
      value: 'Hakim Azahar',
      label: 'Hakim Azahar'
    }
  ]

  return (
    <Form onSubmit={handleSubmit} className='CreateTemplateClass--form'>
      <TextField
        select
        required
        value={typeOfClass}
        label='Type of class'
        onChange={(e) => setTypeOfClass(e.target.value)}
        fullWidth
        style={{ marginTop: '10px', marginBottom: '10px' }}
      >
        {typesOfClasses.map((option) => (
          <MenuItem key={option.value} value={option.value}>
            {option.label}
          </MenuItem>
        ))}
      </TextField>

      <TextField
        type='time'
        InputProps={{ inputProps: { min: moment().format('08:00'), max: moment().format('23:00') } }}
        required
        value={startTime}
        label='Time'
        onChange={(e) => setStartTime(e.target.value)}
        fullWidth
        style={{ marginBottom: '10px' }}
      />

      <TextField
        select
        required
        value={day}
        label='Type of class'
        defaultValue={props.day}
        onChange={(e) => setDay(e.target.value)}
        fullWidth
        style={{ marginBottom: '10px' }}
      >
        {weekdays.map((option) => (
          <MenuItem key={option.value} value={option.value}>
            {option.label}
          </MenuItem>
        ))}
      </TextField>

      <TextField
        type='number'
        InputProps={{ inputProps: { min: 15, max: 120, step: 15 } }}
        required
        value={duration}
        label='Duration (min)'
        onChange={(e) => setDuration(e.target.value)}
        fullWidth
        style={{ marginBottom: '10px' }}
      />

      <TextField
        select
        required
        value={venue}
        label='Venue'
        onChange={(e) => setVenue(e.target.value)}
        fullWidth
        style={{ marginBottom: '10px' }}
      >
        {venues.map((option) => (
          <MenuItem key={option.value} value={option.value}>
            {option.label}
          </MenuItem>
        ))}
      </TextField>

      <TextField
        select
        required
        value={instructor}
        label='Instructor'
        onChange={(e) => setInstructor(e.target.value)}
        fullWidth
        style={{ marginBottom: '10px' }}
      >
        {instructors.map((option) => (
          <MenuItem key={option.value} value={option.value}>
            {option.label}
          </MenuItem>
        ))}
      </TextField>

      <TextField
        InputProps={{ inputProps: { min: 1, max: 20 } }}
        type='number'
        required
        value={maxParticipants}
        label='Max Participants'
        onChange={(e) => setMaxParticipants(e.target.value)}
        fullWidth
        style={{ marginBottom: '10px' }}
      />

      <Button
        disabled={!typeOfClass || !day || !startTime ? true : null}
        type='submit'
        width='209px'
        margin=' 10px 0'
        text={isLoading
          ? <CircularProgress className='loader' color='secondary' />
          : 'Add Class'}
        onClick={handleSubmit}
      />
    </Form>
  )
}

export default CreateTemplateClass
