import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 297,
  },

  cssLabel: {
    color : '#D8D8D8 !important',
},


  cssOutlinedInput: {
    '&$cssFocused $notchedOutline': {
      borderColor: '#FFBA00 !important',
    }
  },

  cssFocused: {
    color : '#fff',
  },

  cssFocusedLabel: {
    color : '#FFBA00',
  },

  notchedOutline: {
    borderWidth: '1px',
    borderColor: '#D8D8D8 !important'
  },

  notchedLabel: {
    color: '#D8D8D8 !important'
  },

});

class ValidField extends React.Component {
  state = {
    email: 'InputMode',
  };

  handleChange = email => event => {
    this.setState({
      [email]: event.target.value,
    });
  };

  render() {
    const { classes } = this.props;

    return (
      <form className={classes.container} noValidate autoComplete="off">
        <TextField
          id="standard-name"
          label={this.props.label}
          className={classes.textField}
          value={this.state.name}
          onChange={this.handleChange('email')}
          margin="normal"
          variant="outlined"
          InputLabelProps={{
            classes: {
              root: classes.cssLabel,
              focused: classes.cssFocused,
            },
          }}
          InputProps={{
            classes: {
              root: classes.cssOutlinedInput,
              focused: classes.cssFocused,
              notchedOutline: classes.notchedOutline,
            },
            inputMode: "numeric"
          }}
        />
      </form>
    );
  }
}

ValidField.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ValidField);
