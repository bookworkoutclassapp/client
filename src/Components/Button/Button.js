import React from 'react'
import Btn from '@material-ui/core/Button'

function Button (props) {
  return (
    <>
      <Btn
        type={props.type}
        disabled={props.disabled}
        startIcon={props.startIcon}
        style={{
          width: props.width,
          height: 52,
          borderRadius: 100,
          backgroundColor: 'primary',
          color: '#071623',
          fontWeight: 700,
          margin: props.margin
        }}
        variant='contained'
        color='primary'
        onClick={props.onClick}
      >
        {props.text}
      </Btn>
    </>
  )
}

export default Button
