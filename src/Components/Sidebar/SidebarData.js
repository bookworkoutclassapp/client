import { IoIosFitness, IoIosCalendar } from 'react-icons/io'
import { AiOutlineEye } from 'react-icons/ai'

export const SidebarData = [
  {
    title: 'Schedule',
    path: '/adminDashboard/schedule',
    icon: IoIosFitness,
    className: 'nav-text'
  },
  {
    title: 'Weekly Template',
    path: '/adminDashboard/weekly-template',
    icon: IoIosCalendar,
    className: 'nav-text'
  },
  {
    title: 'Customer View',
    path: '/userDashboard/book-class',
    icon: AiOutlineEye,
    className: 'nav-text'
  }
]
