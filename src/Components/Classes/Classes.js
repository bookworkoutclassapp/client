import React, { useState, useEffect } from 'react'
import AdminClass from '../AdminClass/AdminClass'
import './Classes.css'
import { CircularProgress } from '@material-ui/core'
import moment from 'moment'

function UpcomingClasses (props) {
  const [days, setDays] = useState([])
  const [isLoading, setIsLoading] = useState(true)

  const fetchClasses = () => {
    window.fetch(`${process.env.REACT_APP_URL}/classes?${props.url}`)
      .then(res => res.json())
      .then(res => { sortClassesOnStartTime(res); setDays(res); setIsLoading(false) })
  }

  const sortClassesOnStartTime = (arr) => {
    arr.forEach(day => {
      day.classes.sort((a, b) => {
        return moment(a.startTime).format('X') - moment(b.startTime).format('X')
      })
    })
  }

  useEffect(() => {
    fetchClasses()
  }, [])

  const unmountClass = (startTime, classID) => {
    console.log(moment(startTime).format('YYYY-MM-DD'))
    const clone = days.slice()
    const indexDay = days.findIndex(({ day }) => moment(day).format('YYYY-MM-DD') === moment(startTime).format('YYYY-MM-DD'))
    const indexClass = days[indexDay].classes.findIndex(({ _id }) => _id === classID)
    clone[indexDay].classes.splice(indexClass, 1)
    setDays(clone)
  }

  return (
    <>
      {isLoading ? <CircularProgress className='loader' color='secondary' />
        : days.map(item => (
          item.classes.map(subItem => (
            <AdminClass key={subItem._id} id={subItem._id} unmountClass={unmountClass} url={props.url} startTime={subItem.startTime} class={subItem.class} participants={subItem.participants} maxParticipants={subItem.maxParticipants} status={subItem.status} />
          ))
        ))}
    </>
  )
}

export default UpcomingClasses
