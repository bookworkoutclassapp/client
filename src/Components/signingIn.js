import { useEffect } from 'react'
import { useLocation } from 'react-router-dom'
import { useHistory } from 'react-router'

function SigningIn () {
  const location = useLocation()
  const history = useHistory()

  useEffect(() => {
    const searchParams = new URLSearchParams(location.search)
    const storage = {
      isLoggedIn: true,
      userID: searchParams.get('userID'),
      displayName: searchParams.get('displayName'),
      isAdmin: searchParams.get('isAdmin')
    }
    window.localStorage.setItem('user', JSON.stringify(storage))

    if (storage.isAdmin == 'true') {
      history.push('adminDashboard/schedule')
    } else {
      history.push('userDashboard/book-class')
    }
  }, [location, history])

  return (
    'signingIn'
  )
}

export default SigningIn
