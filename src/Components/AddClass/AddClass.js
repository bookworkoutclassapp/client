import React, { useState } from 'react'
import './AddClass.css'
import { AiOutlineClose } from 'react-icons/ai'
import { Link } from 'react-router-dom'
import CreateClass from '../CreateClass/CreateClass'
import CreateTemplateClass from '../CreateTemplateClass/CreateTemplateClass'
import { IoIosAdd } from 'react-icons/io'
import Button from '../Button/Button'

function AddClass (props) {
  const [detailBar, setDetailBar] = useState(false)
  const [path] = useState(window.location.pathname)

  const showDetailBar = () => {
    setDetailBar(!detailBar)
  }

  const addNewClass = () => {
    props.addNewClass()
  }

  return (
    <>
      {path === '/adminDashboard/schedule' ? <Button onClick={showDetailBar} text='Add Class' margin='20px 0' width='307px' /> : <span className='addClass-icon' onClick={showDetailBar}><IoIosAdd className='DayTab--addClassIcon' /></span>}
      <div className={detailBar ? 'AddClass--nav-menu AddClass--active' : 'AddClass--nav-menu'}>
        <ul className='AddClass--nav-menu-items' onClick={showDetailBar}>
          <li className='AddClass--navbar-toggle'>
            <Link to='#' className='AddClass--menu-bars'>
              <AiOutlineClose className='AddClass--closeIcon' />
            </Link>
          </li>
        </ul>
        {path === '/adminDashboard/schedule' ? <CreateClass closeDetailBar={showDetailBar} addNewClass={addNewClass} /> : <CreateTemplateClass closeDetailBar={showDetailBar} day={props.day} />}
      </div>
    </>

  )
}

export default AddClass
