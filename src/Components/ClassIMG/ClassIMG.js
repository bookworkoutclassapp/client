import React, { useState, useEffect } from 'react'
import calisthenicsIMG from './Calisthenics.jpg'
import handstandIMG from './Handstand.jpg'
import plancheIMG from './Planche.jpg'
import handstandPushUpsIMG from './HandstandPushup.jpg'

function ClassIMG (props) {
  const [isCalisthenics, setIsCalisthenics] = useState(false)
  const [isHandstand, setIsHandstand] = useState(false)
  const [isPlanche, setIsPlanche] = useState(false)
  const [isHSPushUps, setIsHSPushUo] = useState(false)

  const setIMG = () => {
    if (props.typeOfClass === 'Calisthenics') {
      setIsCalisthenics(true)
    }
    if (props.typeOfClass === 'Handstand') {
      setIsHandstand(true)
    }
    if (props.typeOfClass === 'Planche') {
      setIsPlanche(true)
    }
    if (props.typeOfClass === 'Handstand Push Ups') {
      setIsHSPushUo(true)
    }
  }

  useEffect(() => {
    setIMG()
  }, [])

  return (
    <>
      {isCalisthenics ? <img alt='Hakim doing calisthenics' src={calisthenicsIMG} /> : null}
      {isHandstand ? <img alt='Hakim doing handstand' src={handstandIMG} /> : null}
      {isPlanche ? <img alt='Hakim doing planche' src={plancheIMG} /> : null}
      {isHSPushUps ? <img alt='Hakim doing handstand push ups' src={handstandPushUpsIMG} /> : null}
    </>
  )
}

export default ClassIMG
