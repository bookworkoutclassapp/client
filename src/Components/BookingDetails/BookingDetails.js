import React, { useState } from 'react'
import './BookingDetails.css'
import { IconContext } from 'react-icons'
import { AiOutlineClose } from 'react-icons/ai'
import { Link } from 'react-router-dom'
import Radio from '@material-ui/core/Radio'
import RadioGroup from '@material-ui/core/RadioGroup'
import FormLabel from '@material-ui/core/FormLabel'
import FormControl from '@material-ui/core/FormControl'
import FormControlLabel from '@material-ui/core/FormControlLabel'

function BookingDetails (props) {
  const [paymentMethod, setPaymentMethod] = useState('bankTransfer')

  const handleRadioChange = (event) => {
    setPaymentMethod(event.target.value)
    console.log(event.target.value)
  }

  return (
    <IconContext.Provider value={{ color: '#fff' }}>
      <div className='BookingDetails--Container'>
        <div className='BookingDetails--Container-Close-Container'>
          <AiOutlineClose className='BookingDetails--Close' onClick={console.log('close')} />
        </div>

        <FormControl component='fieldset'>
          <FormLabel component='legend'>Payment Method</FormLabel>
          <RadioGroup row value={paymentMethod} onChange={handleRadioChange}>
            <FormControlLabel value='bankTransfer' control={<Radio />} label='Bank Transfer' />
            <FormControlLabel value='cash' control={<Radio />} label='Cash' />
          </RadioGroup>
        </FormControl>

      </div>
    </IconContext.Provider>
  )
}

export default BookingDetails
