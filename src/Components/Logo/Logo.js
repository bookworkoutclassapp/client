import React from 'react'
import logo from './Logo.png'
import { Link } from 'react-router-dom'
import style from './Logo.module.css'

function Logo () {
  return (
    <Link to='/'>
      <img className={style.Logo} alt='logo' src={logo} />
    </Link>
  )
}

export default Logo
