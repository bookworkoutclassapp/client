import React from 'react'
import { Redirect, Route, Switch } from 'react-router-dom'
import Schedule from '../views/Schedule/Schedule'
import WeeklyTemplate from '../views/WeeklyTemplate/WeeklyTemplate'
import BookClass from '../views/BookClass/BookClass'
import { TemplateClassesContext, TemplateClassesProvider } from '../Contexts/TemplateClassesContext'

function AdminDashboard () {
  return (
    <TemplateClassesProvider>
      <Switch>
        <Route path='/adminDashboard/schedule' component={Schedule} />
        <Route path='/adminDashboard/weekly-template' component={WeeklyTemplate} />
        <Route path='/userDashboard/book-class' component={BookClass} />
        <Redirect from='/adminDashboard' to='/adminDashboard/schedule' />
      </Switch>
    </TemplateClassesProvider>
  )
}

export default AdminDashboard
