import React from 'react'
import { FiPower } from 'react-icons/fi'
import { Link } from 'react-router-dom'
import { toast } from 'react-toastify'
import './Logout.css'
import 'react-toastify/dist/ReactToastify.css'

toast.configure()
function Nav () {
  const logout = () => {
    window.localStorage.clear()
  }

  return (
    <div className='nav'>
      <Link to='/'>
        <div onClick={logout}>
          <FiPower className='icon logout' />
        </div>
      </Link>
    </div>
  )
}

export default Nav
