import React from 'react'
import style from './MockClass.module.css'

function MockClass (props) {
  return (
    <div className={style.MockClassDiv}>
      <div className={style.MockDivInfo}>
        <p className={style.MockClassTime}>{props.time}</p>
        <p className={style.MockClassClass}>{props.class}</p>
      </div>
      <div className={style.MockDivDiv}>
        <div className={style.MockDivMockTextLong} />
        <div className={style.MockDivMockTextShort} />
      </div>
      <p className={style.MockPlus}>+</p>
    </div>
  )
}

export default MockClass
