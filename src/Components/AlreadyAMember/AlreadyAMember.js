import React from 'react'
import { Link } from 'react-router-dom'
import style from './AlreadyAMember.module.css'

function AlreadyAMember () {
  return (
    <div>
      <p className={style.alreadyAMember}>Already a member?
        <Link to='./sign-in'>
          <span> Sign in</span>
        </Link>
      </p>
    </div>
  )
}

export default AlreadyAMember
