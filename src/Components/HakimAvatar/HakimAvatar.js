import React from 'react'
import HakimIMG from './HakimIMG.png'
import style from './HakimAvatar.module.css'

function HakimAvatar () {
  return (
    <div>
      <img className={style.hakimAvatar} alt='hakim avatar' src={HakimIMG} />
    </div>
  )
}

export default HakimAvatar
