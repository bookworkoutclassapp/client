import React, { useState } from 'react'
import { bookClassRequest, cancelClassRequest, changeClassStatus } from '../../apiCalls'
import { toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import moment from 'moment'
import { AiFillPlusCircle, AiFillCheckCircle } from 'react-icons/ai'
import './Class.css'
import ClassIMG from '../../Components/ClassIMG/ClassIMG'

function Class (props) {
  const [isLoading, setIsLoading] = useState(false)
  const [noOfPeople, setNoOfPeople] = useState(props.participants.length)
  const [isClassedConfirmed, setIsClassedConfirmed] = useState(props.status)

  const isAlreadyBooked = (props) => {
    const userID = JSON.parse(window.localStorage.getItem('user')).userID
    const isBooked = props.participants.filter(participant => {
      if (participant.userID === userID) {
        return true
      } else {
        return false
      }
    })
    return !!isBooked[0]
  }

  const notifyError = (message) => {
    toast.error(message, {
      autoClose: 2000
    })
  }

  const bookClass = async () => {
    const user = JSON.parse(window.localStorage.getItem('user'))
    const bookingDetails = {
      classID: props.id,
      userID: user.userID,
      userDisplayName: user.displayName,
      paymentStatus: false
    }

    setIsLoading(true)
    const bookClassResponse = await bookClassRequest(bookingDetails)
    setIsLoading(false)
    if (bookClassResponse.status === 201) {
      props.updateDaysAdd(props.id, props.startTime)
      setNoOfPeople(noOfPeople + 1)
      if (noOfPeople + 1 >= 3) {
        setIsClassedConfirmed('confirmed')
        await changeClassStatus({ classID: props.id })
      }
    }
    if (bookClassResponse.status === 400) {
      notifyError(bookClassResponse.error)
    }
    if (bookClassResponse.status === 405) {
      notifyError(bookClassResponse.data.message)
    }
  }

  const cancelClass = async () => {
    const user = JSON.parse(window.localStorage.getItem('user'))
    const bookingDetails = {
      classID: props.id,
      userID: user.userID
    }
    setIsLoading(true)
    const cancelClassResponse = await cancelClassRequest(bookingDetails)
    setIsLoading(false)
    if (cancelClassResponse.status === 200) {
      props.updateDaysRemove(props.id, props.startTime)
      // setIsClassBooked(false)
      setNoOfPeople(noOfPeople - 1)
    }
    if (cancelClassResponse.status === 400) {
      notifyError('Whoops, something went wrong!')
    }
  }
  return (

    <div className='Class--classDiv'>
      <div className='Class--imgDiv'>
        <ClassIMG typeOfClass={props.class} />
      </div>
      <div className='Class--divInfo'>
        <div className='Class--timeDiv'>
          <p className='Class--time'>{moment(props.startTime).format('HH:mm')}</p>
        </div>
        <div className='Class--details'>
          <p className='Class--class'>{props.class}</p>
          <p className='Class--venue'>{props.venue} - {props.duration} min</p>
          <p>{props.instructor}</p>
        </div>
        <div className='Class--book'>
          {isAlreadyBooked(props) === false
            ? <AiFillPlusCircle className='Class--bookButtonIcon' disabled={isLoading} onClick={bookClass} />
            : <AiFillCheckCircle className='Class--bookButtonIcon Class--bookedClassIcon' disabled={isLoading} onClick={cancelClass} />}
          <p className='Class--participants'>{noOfPeople}/{props.maxParticipants}</p>
        </div>
        {isClassedConfirmed === 'confirmed'
          ? <div className='Class--status Class--confirmed'>confirmed</div>
          : <div className='Class--status'>unconfirmed</div>}
      </div>
    </div>
  )
}

export default Class
