import React, { useState, useEffect, useContext } from 'react'
import './DayTab.css'
import { IoMdArrowDropdown, IoMdArrowDropup } from 'react-icons/io'
import AddClass from '../AddClass/AddClass'
import { TemplateClassesContext } from '../../Contexts/TemplateClassesContext'
import TemplateClass from '../../Components/TemplateClass/TemplateClass'

function DayTab (props) {
  const [day, setDay] = useState(false)
  const [templateClasses, setTemplateClasses] = useContext(TemplateClassesContext)

  const showDay = () => {
    setDay(!day)
  }

  const filterClasses = () => {
    return templateClasses.filter(row => row.day === props.day).sort((a, b) => {
      return a.startTime - b.startTime
    })
  }

  const classesForThatDay = filterClasses()

  useEffect(() => {
    if (classesForThatDay.length > 0) {
      setDay(true)
    }
  }, [templateClasses])

  return (
    <>
      <div className='Schedule--tab'><span onClick={showDay}>{props.day}{day ? <IoMdArrowDropup color='#FFBA00' /> : <IoMdArrowDropdown color='#FFBA00' />}</span><AddClass day={props.day} /></div>
      {day ? classesForThatDay.map(item => (
        <TemplateClass key={item._id} id={item._id} startTime={item.startTime} class={item.class} venue={item.venue} instructor={item.instructor} duration={item.duration} maxParticipants={item.maxParticipants} />
      )) : null}

    </>
  )
}

export default DayTab
