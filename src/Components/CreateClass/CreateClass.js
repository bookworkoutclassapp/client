import React, { useState } from 'react'
import Form from 'react-bootstrap/Form'
import './CreateClass.css'
import { addClassRequest } from '../../apiCalls'
import { toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import { CircularProgress } from '@material-ui/core'
import TextField from '@material-ui/core/TextField'
import MenuItem from '@material-ui/core/MenuItem'
import Button from '../Button/Button'
import moment from 'moment'

function CreateClass (props) {
  const [typeOfClass, setTypeOfClass] = useState('')
  const [date, setDate] = useState('')
  const [time, setTime] = useState('')
  const [duration, setDuration] = useState(60)
  const [venue, setVenue] = useState('Siam Strength')
  const [instructor, setInstructor] = useState('Hakim Azahar')
  const [status, setStatus] = useState('unconfirmed')
  const [maxParticipants, setMaxParticipants] = useState(10)
  const [isLoading, setIsLoading] = useState(false)

  const notifyError = (message) => {
    toast.error(message, {
      autoClose: 2000
    })
  }

  const handleSubmit = async (event) => {
    setIsLoading(true)
    event.preventDefault()
    const body = {
      class: typeOfClass,
      startTime: date + ' ' + time,
      duration: duration,
      venue: venue,
      instructor: instructor,
      status: status,
      maxParticipants: maxParticipants
    }
    const response = await addClassRequest(body)
    if (response.status === 201) {
      props.addNewClass()
      props.closeDetailBar()
      setIsLoading(false)
      setTypeOfClass('')
      setDate('')
      setTime('')
    } else {
      notifyError('Something went wrong!')
    }
  }

  const typesOfClasses = [
    {
      value: 'Calisthenics',
      label: 'Calisthenics'
    },
    {
      value: 'Handstand',
      label: 'Handstand'
    },
    {
      value: 'Handstand Push Ups',
      label: 'Handstand Push Ups'
    },
    {
      value: 'Planche',
      label: 'Planche'
    }
  ]

  const venues = [
    {
      value: 'Online',
      label: 'Online'
    },
    {
      value: 'Siam Strength',
      label: 'Siam Strength'
    }
  ]

  const instructors = [
    {
      value: 'Hakim Azahar',
      label: 'Hakim Azahar'
    }
  ]

  const statuses = [
    {
      value: 'unconfirmed',
      label: 'Unconfirmed'
    },
    {
      value: 'confirmed',
      label: 'Confirmed'
    }
  ]

  return (
    <Form onSubmit={handleSubmit} className='CreateClass--form' autocomplete='off'>
      <TextField
        select
        required
        value={typeOfClass}
        label='Type of class'
        onChange={(e) => setTypeOfClass(e.target.value)}
        fullWidth
        style={{ marginBottom: '10px' }}
      >
        {typesOfClasses.map((option) => (
          <MenuItem key={option.value} value={option.value}>
            {option.label}
          </MenuItem>
        ))}
      </TextField>
      <TextField
        type='date'
        InputProps={{ inputProps: { min: moment().format('YYYY-MM-DD'), max: moment().add(14, 'days').format('YYYY-MM-DD') } }}
        required
        value={date}
        label='Date'
        onChange={(e) => setDate(e.target.value)}
        fullWidth
        style={{ marginBottom: '10px' }}
      />
      <TextField
        type='time'
        InputProps={{ inputProps: { min: moment().format('08:00'), max: moment().format('23:00') } }}
        required
        value={time}
        label='Time'
        onChange={(e) => setTime(e.target.value)}
        fullWidth
        style={{ marginBottom: '10px' }}
      />
      <TextField
        type='number'
        InputProps={{ inputProps: { min: 15, max: 120, step: 15 } }}
        required
        value={duration}
        label='Duration (min)'
        onChange={(e) => setDuration(e.target.value)}
        fullWidth
        style={{ marginBottom: '10px' }}
      />

      <TextField
        select
        required
        value={venue}
        label='Venue'
        onChange={(e) => setVenue(e.target.value)}
        fullWidth
        style={{ marginBottom: '10px' }}
      >
        {venues.map((option) => (
          <MenuItem key={option.value} value={option.value}>
            {option.label}
          </MenuItem>
        ))}
      </TextField>

      <TextField
        select
        required
        value={instructor}
        label='Instructor'
        onChange={(e) => setInstructor(e.target.value)}
        fullWidth
        style={{ marginBottom: '10px' }}
      >
        {instructors.map((option) => (
          <MenuItem key={option.value} value={option.value}>
            {option.label}
          </MenuItem>
        ))}
      </TextField>

      <TextField
        InputProps={{ inputProps: { min: 1, max: 20 } }}
        type='number'
        required
        value={maxParticipants}
        label='Max Participants'
        onChange={(e) => setMaxParticipants(e.target.value)}
        fullWidth
        style={{ marginBottom: '10px' }}
      />

      <TextField
        select
        required
        value={status}
        label='Status'
        onChange={(e) => setStatus(e.target.value)}
        fullWidth
        style={{ marginBottom: '10px' }}
      >
        {statuses.map((option) => (
          <MenuItem key={option.value} value={option.value}>
            {option.label}
          </MenuItem>
        ))}
      </TextField>

      <Button
        disabled={!typeOfClass || !date || !time ? true : null}
        type='submit'
        width='209px'
        margin=' 10px 0'
        text={isLoading
          ? <CircularProgress className='loader' color='secondary' />
          : 'Add Class'}
        onClick={handleSubmit}
      />
    </Form>
  )
}

export default CreateClass
