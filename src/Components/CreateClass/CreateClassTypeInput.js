export const classFormInput = (classes, onChange, typeOfClass) => {
  return {
    type: 'text',
    label: 'Class',
    defaultValue: typeOfClass,
    placeholder: 'Class',
    name: 'class',
    onChange: onChange,
    options: classes,
    selectLabel: '--select class--',
    selectNone: '-none-'
  }
}
