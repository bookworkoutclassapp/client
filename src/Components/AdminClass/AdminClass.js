import React, { useState } from 'react'
import './AdminClass.css'
import moment from 'moment'
import { changeClassStatus, deleteClassRequest } from '../../apiCalls'
import 'react-toastify/dist/ReactToastify.css'
import { toast } from 'react-toastify'

function AdminClass (props) {
  const [noOfPeople] = useState(props.participants.length)
  const [classStatus, setClassStatus] = useState(props.status)

  const notifyError = (message) => {
    toast.error(message, {
      autoClose: 2000
    })
  }

  const confirmClass = async () => {
    const response = await changeClassStatus({ classID: props.id })
    if (response.status === 200) {
      setClassStatus('confirmed')
    } else {
      notifyError('Something went wrong!')
    }
  }

  const cancelClass = async () => {
    const response = await deleteClassRequest({ classID: props.id })
    if (response.status === 200) {
      props.unmountClass(props.startTime, props.id)
    } else {
      notifyError('Something went wrong!')
    }
  }

  return (
    <div className='AdminClass--ClassDiv'>
      <div className='AdminClass--infoDiv'>
        <p className='AdminClass--date'>{moment(props.startTime).format('dddd')} {moment(props.startTime).format('DD MMM')}</p>
        <p className='AdminClass--time'>{moment(props.startTime).format('HH:mm')}</p>
        <p className='AdminClass--class'>{props.class}</p>
        <p className='AdminClass--noOfParticpantsText'>No. of Participants: </p>
        <p className='AdminClass--noOfParticpants'>{noOfPeople} / {props.maxParticipants}</p>
        <ul className='AdminClass--participants'>
          {props.participants.map(item => (
            <li className='AdminClass--participants-li' key={item.userID}>{item.displayName}</li>
          ))}
        </ul>
      </div>
      {props.url === 'past=true'
        ? null
        : <div className='AdminClass-actionDiv'>
          {classStatus === 'unconfirmed'
            ? <div className='AdminClass--actionBtn AdminClass--status AdminClass--cursor' onClick={confirmClass}>Confirm?</div>
            : <div className='AdminClass--actionBtn AdminClass--status AdminClass--confirmed'>Confirmed</div>}
          <div className='AdminClass--actionBtn AdminClass--cancel AdminClass--cursor' onClick={cancelClass}>Cancel</div>

        </div>}
    </div>

  )
}

export default AdminClass
