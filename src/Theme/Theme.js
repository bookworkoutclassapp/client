import { createMuiTheme } from '@material-ui/core/styles'

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#FFBA00'
    },
    secondary: {
      main: '#fff'
    }
  },
  overrides: {
    MuiInputLabel: {
      root: {
        color: '#D8D8E1'
      }
    },
    MuiOutlinedInput: {
      root: {
        color: 'white',
        '&:hover $notchedOutline': {
          borderColor: 'white'
        },
        '& $notchedOutline': {
          borderColor: '#D8D8E1',
          marginBottom: 0
        }
      }
    },
    MuiSvgIcon: {
      root: {
        color: '#071623'
      }
    }
  },
  props: {
    MuiTextField: {
      variant: 'outlined',
      size: 'small',
      marginBottom: 20,
      InputLabelProps: {
        shrink: 'true'
      }
    }
  }
})

export default theme
