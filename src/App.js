import React from 'react'
import './App.css'
import { BrowserRouter as Router, Redirect, Route, Switch } from 'react-router-dom'
import { AuthProvider } from './Contexts/authContext'
import Home from './views/Home/Home'
import SignIn from './views/SignIn/SignIn'
import ForgotPassword from './views/ForgotPassword/ForgotPassword'
import Register from './views/Register/Register'
import Schedule from './views/Schedule/Schedule'
import WeeklyTemplate from './views/WeeklyTemplate/WeeklyTemplate'
import UserDashboard from './Components/userDashboard'
import AdminDashboard from './Components/adminDashboard'
import SigningIn from './Components/signingIn'
import { ThemeProvider } from '@material-ui/core/styles'
import theme from './Theme/Theme'

// isLoggedIn === true and isAdmin === false
const UserPrivateRoute = ({ ...rest }) => {
  const userVariable = JSON.parse(window.localStorage.getItem('user'))
  const isLoggedIn = userVariable?.isLoggedIn

  return isLoggedIn ? (
    <UserDashboard {...rest} />
  ) : (
    <Redirect
      to={{
        pathname: '/sign-in'
      }}
    />
  )
}

// isLoggedIn === true and isAdmin === true
const AdminPrivateRoute = ({ ...rest }) => {
const userVariable = JSON.parse(window.localStorage.getItem('user'))
const isLoggedIn = userVariable?.isLoggedIn
const isAdmin = userVariable?.isAdmin

// TODO add a flashmessage for not being authorized.
  return isLoggedIn && isAdmin ? (
    <AdminDashboard {...rest} />
  ) : (
    <Redirect
    
      to={{
        pathname: '/sign-in'
      }}
    />
  )
}

function App () {
  return (
    <ThemeProvider theme={theme}>
    <AuthProvider>
        <Router>
          <div className='App'>
            <Switch>
              <Route path='/' exact component={Home} />
              <Route path='/sign-in' component={SignIn} />
              <Route path='/forgot-password' component={ForgotPassword} />
              <Route path='/register' component={Register} />
              <Route path='/signing-in' component={SigningIn} />
              <Route path='/userDashboard' component={UserPrivateRoute} />
              <Route path='/adminDashboard' component={AdminPrivateRoute} />
              <Route path='/schedule' component={Schedule} />
              <Route path='/weekly-template' component={WeeklyTemplate} />
            </Switch>
          </div>
        </Router>
    </AuthProvider>
    </ThemeProvider>
  )
}

export default App
