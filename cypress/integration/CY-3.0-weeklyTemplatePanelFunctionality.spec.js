describe('Reach landing page:', () => {
  it('logging in', () => {
    cy.visit('http://localhost:3000')
      .get('button').contains('Book Class').click()
      .url().should('include', '/sign-in')
      .get('input[id=email]').type('admin@admin.admin')
      .get('input[id=password]').type('password')
      .get('button[type=submit]').click()
  })

  it('navigate to weekly template', () => {
    cy.get('div[class=sidebar').children(1).click()
      .get('a').contains('Weekly Template').click()
  })

  it('check for element specific to Weekly Template view', () => {
    cy.get('div[class=Schedule--tab').contains('Monday')
      .get('div[class=Schedule--tab').contains('Tuesday')
      .get('div[class=Schedule--tab').contains('Wednesday')
      .get('div[class=Schedule--tab').contains('Thursday')
      .get('div[class=Schedule--tab').contains('Friday')
      .get('div[class=Schedule--tab').contains('Saturday')
      .get('div[class=Schedule--tab').contains('Sunday')
  })

  it('add a class', () => {
    cy.get('div[class=Schedule--tab]').contains('Monday').get('span[class=addClass-icon]:first').children().click()
      .get('input[id=class]:first').type('Handstand')
      .get('input[id=startTime]:first').type('07:59')
      .get('button[type=submit]:first').click()
  })

  it('delete a class', () => {
    cy.get('div[class=TemplateClass--ClassDiv]').contains('07:59').parent().parent().parent().children().contains('Delete').click()
  })
})
