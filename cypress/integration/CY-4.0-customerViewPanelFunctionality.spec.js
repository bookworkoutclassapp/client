import 'cypress-localstorage-commands'
const moment = require('moment')
const todaysDate = moment().format('DD')

describe('Reach landing page:', () => {
  it('logging in', () => {
    cy.visit('http://localhost:3000')
      .get('button').contains('Book Class').click()
      .url().should('include', '/sign-in')
      .get('input[id=email]').type('admin@admin.admin')
      .get('input[id=password]').type('password')
      .get('button[type=submit]').click()
  })

  it('navigate to customer view', () => {
    cy.get('div[class=sidebar').children(1).click()
      .get('a').contains('Customer View').click()
  })

  it('check for element specific to Customer View', () => {
    cy.setLocalStorage('user', '{"isLoggedIn":true,"userID":"60b44c75e2af44a18b042b90","displayName":"Admin","isAdmin":true}')
      .url().should('include', '/userDashboard/book-class')
      .get('ul[class=BookClass--days]')
  })
})
