// const todaysDate = Cypress.moment().format('YYYY-MM-DD')
const moment = require('moment')
const todaysDate = moment().format('YYYY-MM-DD')

describe('Reach landing page:', () => {
  it('logging in', () => {
    cy.visit('http://localhost:3000')
      .get('button').contains('Book Class').click()
      .url().should('include', '/sign-in')
      .get('input[id=email]').type('admin@admin.admin')
      .get('input[id=password]').type('password')
      .get('button[type=submit]').click()
  })

  it('CY-2.1 - Check for element specific to Schedule view', () => {
    cy.get('div[class=Schedule--tab')
      .get('button[class=AddClass--btn')
  })

  it('CY-2.2 - Add a class', () => {
    cy.get('button[class=AddClass--btn]').click()
      .get('input[id=class]').type('Handstand')
      .get('input[id=date]').type(todaysDate)
      .get('input[id=startTime]').type('07:59')
      .get('button[type=submit]').click()
  })

  it('CY-2.3 - Confirm a class', () => {
    cy.get('div').contains('07:59').parent().parent().children().contains('Confirm?').click()
  })

  it('CY-2.4 - Check for participants', () => {
    cy.get('div').contains('07:59').parent().children().contains('No. of Participants: ')
  })

  it('CY-2.5 - Delete a class', () => {
    cy.get('div').contains('07:59').parent().parent().children().contains('Cancel').click()
  })
})
