import 'cypress-localstorage-commands'
const moment = require('moment')
const todaysDate = moment().format('DD')

describe('Reach landing page:', () => {
  it('logging in', () => {
    cy.visit('http://localhost:3000')
      .get('button').contains('Book Class').click()
      .url().should('include', '/sign-in')
      .get('input[id=email]').type('customer@customer.customer')
      .get('input[id=password]').type('password')
      .get('button[type=submit]').click()
  })

  it('check for element specific to Customer View', () => {
    cy.url().should('include', '/userDashboard/book-class')
      .get('ul[class=BookClass--days]')
  })

  it('book a class', () => {
    cy.setLocalStorage('user', '{"isLoggedIn":true,"userID":"60b48c9be2af44a18b042bca","displayName":"Customer","isAdmin":false}')
      .get('div[class=Class--book]').first().click()
  })

  it('cancel a class', () => {
    cy.setLocalStorage('user', '{"isLoggedIn":true,"userID":"60b48c9be2af44a18b042bca","displayName":"Customer","isAdmin":false}')
      .get('div[class=Class--book]').first().click()
  })
})
