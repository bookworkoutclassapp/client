const randomUserName = 'User' + Math.floor(Math.random() * 1000000)

describe('Reach landing page', () => {
  it('landing page successfully loads', () => {
    cy.visit('http://localhost:3000')
  })

  it('you get redirected to /sign-in when clicking "BOOK CLASS"', () => {
    cy.get('button').contains('Book Class').click()
    cy.url().should('include', '/sign-in')
  })
})

describe('CY-1.1 - Try to login in with a user that does not exist (unsuccessfully)', () => {
  it('type in email', () => {
    cy.get('input[id=email]').type(randomUserName + '@mail.com')
  })

  it('type in password', () => {
    cy.get('input[id=password]').type(randomUserName)
  })

  it('click sign in button', () => {
    cy.get('button[type=submit]').click()
  })

  it('error message appears since user do not exist', () => {
    cy.get('div[role="alert"]').should('be.visible').contains('No matching user!')
  })
})

describe('Reach register page', () => {
  it('"/sign-in" contains a link to become a member', () => {
    cy.get('p[class="notAMember"').contains('a').click()
  })

  it('you get redirected to /register when clicking "Become one today"', () => {
    cy.url().should('include', '/register')
  })
})

describe('CY-1.2 - Register a user (successfully)', () => {
  it('register button is disabled when fields are empty', () => {
    cy.get('button[type="submit"]').should('be.disabled')
  })

  it('type in email', () => {
    cy.get('input[id=email]').type(randomUserName + '@mail.com')
  })

  it('type in password', () => {
    cy.get('input[id=password]').type(randomUserName)
  })

  it('type in repeat password', () => {
    cy.get('input[id=repeatPassword]').type(randomUserName)
  })

  it('type in nickname', () => {
    cy.get('input[id=nickname]').type(randomUserName)
  })

  it('type in instagram', () => {
    cy.get('input[id=instagram]').type(randomUserName)
  })

  it('click register button', () => {
    cy.get('button[type=submit]').click()
  })

  it('success message appears', () => {
    cy.get('div[role="alert"]').should('be.visible').contains('Account created! You can now login')
  })

  it('redirect to /sign-in"', () => {
    cy.url().should('include', '/sign-in')
  })
})

describe('CY-1.3 - Sign in with correct email and incorrect password (unsuccessfully)', () => {
  it('navigate to sign in user', () => {
    cy.get('input[id=email]').clear().type(randomUserName + '@mail.com')
  })

  it('type in password', () => {
    cy.get('input[id=password]').clear().type('wrong password')
      .wait(1500)
  })

  it('click sign in button', () => {
    cy.get('button[type=submit]').click()
  })

  it('error message appears since user do not exist', () => {
    cy.get('div[role="alert"]').contains('No matching user!')
  })
})

describe('CY-1.4 - Sign in with incorrect email and correct password (unsuccessfully)', () => {
  it('type in email', () => {
    cy.get('input[id=email]').clear().type('incorrect@mail.com')
  })

  it('type in password', () => {
    cy.get('input[id=password]').clear().type(randomUserName)
  })

  it('click sign in button', () => {
    cy.get('button[type=submit]').click()
  })

  it('error message appears since user do not exist', () => {
    cy.get('div[role="alert"]').contains('No matching user!')
    cy.wait(1500)
  })
})

describe('CY-1.5 - Sign in with correct email and correct password (successfully)', () => {
  it('type in email', () => {
    cy.get('input[id=email]').clear().type(randomUserName + '@mail.com')
  })

  it('type in password', () => {
    cy.get('input[id=password]').clear().type(randomUserName)
  })

  it('click sign in button', () => {
    cy.get('button[type=submit]').click()
  })

  it('you get redirected to /book-class', () => {
    cy.url().should('include', '/book-class')
  })
})
